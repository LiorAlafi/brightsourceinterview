#pragma once

#include <vector> // the actual board
#include <memory> // using unique_ptr

using std::vector;
using std::unique_ptr;


//-----FWD declarations-------------------------
class IFunc;

//--- STRUCTS----------------------------------------
struct Coordinate
{
	int x = -1, 
		y = -1;
};

struct Limit
{
	int m_min;
	int m_max;

	Limit(int min,int max);

};
//-----------------------------------------------------------
/*
this class represent cartesian plane (or board)
methods:
=========
board - ctor, sets a valid board/cartesian plane
draw - allows you to draw a function on the 
show - shows the plane with everything drawn on it

*/
class Board final
{
public:
	/* CTOR
	@input: 
	minX - minimum x value
	maxX - maximum x value
	minY - minimum y value 
	maxY - maximum y value
	resX - x resolution
	resY - y resolution 
	*/
	Board(int minX = -10, 
		int maxX = 10, 
		int minY = -10,  
		int maxY = 10,    
		int resX = 3,  
		int resY = 3);  
	//..................................................
	
	
	/*
	plot a function on a board 

	@input:
	f - a functor (gets and returns double) 
	mark - function line will be plotted with this character
	
	*/
	void draw(IFunc &f, const char mark = 'x');
	//.....................................................

	/*
	shows the board to the user

	*/
	void show() const;


private:
	/*
	translate two values (x, f(x)) to a coordinate on this board

	@input:
	x - double value (- Domain
	y - double value (- Range

	@output
	coordinate (x,y) in our board representation system
	*/
	Coordinate valueMapping(double x,double y);
	
	/*
	produce all the x values that are between the horizontal limits 
	*/
	unique_ptr<vector<double>> getX();
	
	/*
	translate double value into integer in our system:
	formula: g(t) =  step*t - step*min
	 
	@input:
	value: t in our formula
	axis: since resolution nor limits are the same for x and y axis 
	min,step are defined by the selected axis 


	@output: integer value in our system of representation
	*/
	int map2int(const double value, const char axis = 'x');
	
	Limit m_horizontal, 
		  m_vertical;


	int m_stepX,
		m_stepY,
		m_horizontalSize, 
		m_verticalSize;

	vector<vector<char>> m_board;
	
	const int m_percisionFactor = 50; // percision factor - trying to make sure there're no "holes" in the function
									  // really bad way but best thing I can think of that doesn't require
									  // post processing the coordinates and apply some linear equasion between them
								      // trully crappy idea...
};