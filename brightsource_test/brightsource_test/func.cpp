//-----HEADERS-------------------------
#include "stdafx.h"
#include "func.h"
#include <limits>
#include <stdexcept> 
//..................................................



/* CTOR
parse coef string and make a polynome out of it;
@input: coefficient string for example "1 2 3" represent the polynome: 1+2x+3x^
@output:
*/
Polynome::Polynome(const std::string & coef)
{
	if (coef.find_first_not_of(" .-0123456789") != coef.npos)
		return; // 


	auto tmp = coef;
		
	trim(tmp);
	// tokenize str and turn to coefficients
	while (!tmp.empty())
	{
		m_coeff.emplace_back(atof(tmp.c_str()));
		auto spaceloc = tmp.find(" ");
		
		if (spaceloc == tmp.npos)
			break;

		tmp = tmp.substr(spaceloc+1);
		trim(tmp);
	}	
}
//...............................................................................
/*
parse coef string and make a polynome out of it;
@input: x - a value in the domain
@output: f(x) //NaN is irrelevant in polynomes so always expect a result
*/
double Polynome::operator()(double x)
{
	double sum = 0;

	//using this type of for because i want to use i as power indicator
	for (int i = 0; i < (int)m_coeff.size(); ++i)
		sum += m_coeff[i] * pow(x, i);

	return sum;
}
