// stdafx.cpp : source file that includes just the standard includes
// brightsource_test.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file


void trim(std::string &s) {
	auto start = s.find_first_not_of(" \t\n");
	if (start != s.npos)
		s = s.substr(start);

	auto end = s.find_last_not_of(" \t\n");
	if (end != s.npos)
	{
		s.erase(end + 1);
	}
}