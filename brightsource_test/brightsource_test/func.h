#pragma once

//-----HEADERS-------------------------
#include <string>
#include <vector>

using std::string;
using std::vector;


//------ADT'S--------------------

class IFunc {
/* 
function interface:
====================
this is actually a functor 
and since domain is usually part of R (REAL) 
and same goes for range I chose double.

this can be changed with template but already I couldn't solve it in less than an hour.
(the entire interview question)

methods:
========
operator(): return double and get a double
please adhere to the following convention and make sure that

if x DOESN'T belong to domain return std::numeric_limits<double>::quiet_NaN(); 
(found in <limit>)

*/

public:
	/*
	@input: x  - value in the domain
	@output: f(x)
	*/
	virtual double operator()(double x) = 0;
};

//-------------------------------------------------------------------------------------------------

class Polynome: public IFunc {
/*
represent a polynomical from any degree , implements IFunc

*/
public:
	/* CTOR
	parse coef string and make a polynome out of it;
	to avoid confusion use ctors like described in scott meyer's book "effective C++"
	meaning: Polynome p{....}

	@input: coefficient string for example "1 2 3" represent the polynome: 1+2x+3x^
	@output:
	*/
	Polynome(const std::string &coef);

	/* 
	parse coef string and make a polynome out of it;
	@input: x - a value in the domain
	@output: f(x) //NaN is irrelevant in polynomes so always expect a result
	*/
	double operator()(double x) override;

private:
	vector<double> m_coeff;

};