#include "stdafx.h"
#include "func.h"
#include <cmath>
#include "board.h"
using std::make_unique;

/* CTOR
@input:
minX - minimum x value
maxX - maximum x value
minY - minimum y value
maxY - maximum y value
resX - x resolution
resY - y resolution
*/
Board::Board(int minX, int maxX, int minY, int maxY, int resX,int resY) : m_horizontal(minX, maxX),
																m_vertical(minY, maxY),
																m_stepX(resX),
																m_stepY(resY)
{
	m_horizontalSize = m_stepX*(maxX - minX);
	m_verticalSize = m_stepY*(maxY - minY);

	for (int i = 0; i < m_verticalSize ; ++i) {
		vector<char> tmp;
		for (int j = 0; j < m_horizontalSize; ++j)
		{
			// create axis...
			if (i == map2int(0.0,'y') -1) 
				tmp.emplace_back('_');
			else if (j == map2int(0.0,'x'))
				tmp.emplace_back('|');
			else  //default
				tmp.emplace_back('-');
		}
		m_board.emplace_back(tmp);
	}
	
}
//-----------------------------------------------------------------------------
/*
translate two values (x, f(x)) to a coordinate on this board

@input:
x - double value (- Domain
y - double value (- Range

@output
coordinate (x,y) in our board representation system
*/
Coordinate Board::valueMapping(double x, double y)
{
	Coordinate coord;
	
	//check if value is legal
	if (x != std::numeric_limits<double>::quiet_NaN() &&
		y != std::numeric_limits<double>::quiet_NaN())
	{
		coord.x = map2int(x);
		coord.y = map2int(y, 'y');
	}

	return coord;
}
//-----------------------------------------------------------------------------
/*
produce all the x values that are between the horizontal limits
*/
unique_ptr<vector<double>> Board::getX()
{
	auto values = make_unique<vector<double>>(); 
	double step = 1.0 / (m_stepX * m_percisionFactor);

	double x = m_horizontal.m_min;

	while (x <= m_horizontal.m_max) 
	{
		values->emplace_back(x);
		x += step;
	}

	return values;
}
//-----------------------------------------------------------------------------
/*
translate double value into integer in our system:
formula: g(t) =  step*t - step*min

@input:
value: t in our formula
axis: since resolution nor limits are the same for x and y axis
min,step are defined by the selected axis


@output: integer value in our system of representation
*/
int Board::map2int(const double value, const char axis)
{
	double min, step;

	if (axis == 'x')
	{
		min = m_horizontal.m_min;
		step = m_stepX;
	}
	else
	{
		min = m_vertical.m_min;
		step = m_stepY;
	}

	return static_cast<int>(round(step*value - min * step));
}
//-----------------------------------------------------------------------------
/*
plot a function on a board

@input:
f - a functor (gets and returns double)
mark - function line will be plotted with this character

*/
void Board::draw(IFunc & f, const char mark)
{
	auto X = getX();
	vector<Coordinate> coords;

	for (const auto &x : *X)
		coords.emplace_back(valueMapping(x, f(x)));

	for (const auto &coord : coords)
	{
		//in boundries
		if (coord.x >= 0 && coord.x < m_horizontalSize &&
			coord.y >= 0 && coord.y < m_verticalSize)
			m_board[m_verticalSize - coord.y - 1][coord.x] = mark;
	}
}
//-----------------------------------------------------------------------------
/*
shows the board to the user
*/
void Board::show() const
{

	for (int i = 0; i < m_verticalSize; ++i)
	{
		for (int j = 0; j < m_horizontalSize ; ++j)
			std::cout << m_board[i][j];

		std::cout << std::endl;
	}
}
//-----------------------------------------------------------------------------
Limit::Limit(int min, int max)
{
	if (min < max)
	{
		m_min = min;
		m_max = max;
	}
	else
	{
		m_min = max;
		m_max = min;
	}
}
