// brightsource_test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "board.h"
#include "func.h"


int main(int argc, char *argv[])
{
	Board b;
	Polynome line{ "0 1" };
	Polynome pol{ ((argc == 2) ? argv[1] : "0 0 0 1") };
	b.draw(pol);
	b.draw(line, 'c');
	b.show();
	
	system("pause");
	return 0;
}

